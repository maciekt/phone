(function() {
    var openAdd = document.getElementById('addBtn');
    openAdd.addEventListener('click', openEditBox, false);
    var editBox = document.getElementById('editBox');
    var addBtn = document.getElementById('add');
    addBtn.addEventListener('click', prepareData, false);
    var saveBtn = document.getElementById("save");
    saveBtn.addEventListener('click', saveEl, false);
    var newEl = document.getElementById('phoneBody');
    var nrlicznik = 1;
    var licznik = 1;
    var target;


    var name = document.getElementById("name");
    var email = document.getElementById("email");
    var phone = document.getElementById("phone");


    function openEditBox() {
        clearInputs();
        editBox.classList.remove('none');
        addBtn.classList.remove('none');
        saveBtn.classList.add('none');
        console.log("editbox open");
    }

    function addNewPerson(data) {

        editBox.classList.add('none');
        saveBtn.classList.add('none');
        addBtn.classList.remove('none');
        console.log("editbox close")
        console.log("added new person ")
        var cell = [];
        var tableRow = document.createElement('tr');
        tableRow.setAttribute("id", "row-" + licznik);
        tableRow.setAttribute("data-id", licznik)
        newEl.appendChild(tableRow);

        for (var i = 0; i < 6; i++) {
            cell[i] = document.createElement('td');
            tableRow.appendChild(cell[i]);
        }
        var nrText = document.createTextNode(nrlicznik++)
        var photoEl = document.createElement('img');
        photoEl.src = 'assets/person.png'
        var nameEl = document.createTextNode(data.name)
        var phoneEl = document.createTextNode(data.phone);
        var emailEl = document.createTextNode(data.email);
        var btn = document.createElement('input');
        btn.type = "button";
        btn.value = "edit";
        btn.classList.add('btn-xs');
        btn.addEventListener('click', editPerson, false);
        var btnDel = document.createElement('input');
        btnDel.type = "button"
        btnDel.value = "-";
        btnDel.classList.add('btn-xs');
        btnDel.addEventListener('click', delPerson, false);

        cell[0].appendChild(nrText);
        cell[1].appendChild(photoEl);
        cell[2].appendChild(nameEl);
        cell[3].appendChild(phoneEl);
        cell[4].appendChild(emailEl);
        cell[5].appendChild(btn);
        cell[5].appendChild(btnDel);
        console.log(cell.length);
        licznik++;

    }

    function clearInputs() {
        name.value = "";
        email.value = "";
        phone.value = "";
    }
    

    function prepareData(person) {
        var person = {
            name: document.getElementById("name").value,
            email: document.getElementById("email").value,
            phone: document.getElementById("phone").value

        }
        addNewPerson(person);

    }

    function editPerson() {
        editBox.classList.remove('none');
        addBtn.classList.add('none');
        console.log("open edit person box")
        saveBtn.classList.remove('none');
        target = this.parentNode.parentNode;
        var elements = target.getElementsByTagName("td");
        name.value = elements[2].textContent;
        email.value = elements[3].textContent;
        phone.value = elements[4].textContent;

    }

    function saveEl() {
        var elements = target.getElementsByTagName("td");
        var id = target.getAttribute("data-id")
        console.log(id);
        elements[2].innerHTML = name.value;
        elements[3].innerHTML = phone.value;
        elements[4].innerHTML = email.value;
        editBox.classList.add('none');
        saveBtn.classList.add('none');
        clearInputs();
    }

    function delPerson() {
        console.log("remove person")
        editBox.classList.add('none');
        var curentEl = this.parentNode.parentNode;
        curentEl.remove();
        nrlicznik--;

    }
})();
